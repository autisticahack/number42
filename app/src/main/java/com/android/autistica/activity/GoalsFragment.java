package com.android.autistica.activity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.autistica.R;
import com.android.autistica.app.Application;

public final class GoalsFragment extends Fragment {

    TextView message;
    ImageView im_edu;

    private static final String PAGE_POSITION = "PAGE_POSITION";

    private static TypedArray goals_imgs = Application.getApplicationInstance().getResources().obtainTypedArray(R.array.goal_images);
    private static TypedArray goals_msg = Application.getApplicationInstance().getResources().obtainTypedArray(R.array.goal_msg);



    public static GoalsFragment newInstance(int position) {
        GoalsFragment fragment = new GoalsFragment();

        Bundle args = new Bundle();
        args.putInt(PAGE_POSITION, position);
        fragment.setArguments(args);

//        StringBuilder builder = new StringBuilder();
//        for (int i = 0; i < 20; i++) {
//            builder.append(content).append(" ");
//        }
//        builder.deleteCharAt(builder.length() - 1);
//        fragment.mContent = builder.toString();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_goals, container, false);
        int position = getArguments().getInt(PAGE_POSITION);
        message = (TextView) view.findViewById(R.id.tv_fmsg);
        im_edu = (ImageView) view.findViewById(R.id.iv_ficon);

        im_edu.setImageResource(goals_imgs.getResourceId(position, -1));
//        String msg = edu_msg.getString(position);
        message.setText(goals_msg.getString(position));

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
}
