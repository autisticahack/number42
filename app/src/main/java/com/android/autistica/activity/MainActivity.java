package com.android.autistica.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import com.android.autistica.R;


public class MainActivity extends BaseActivity {

    private View progressOverlay;
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);


//        progressOverlay = findViewById(R.id.progressBar);


        ImageButton eduButton = (ImageButton) findViewById(R.id.imageButton1);
        ImageButton worryButton = (ImageButton) findViewById(R.id.imageButton2);
        ImageButton goalsButton = (ImageButton) findViewById(R.id.imageButton4);
        ImageButton commButton = (ImageButton) findViewById(R.id.imageButton3);

        eduButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent otpIntent = new Intent(MainActivity.this, ViewPagerActivity.class);
                startActivity(otpIntent);
            }});

        com.rey.material.widget.Button btn_panic = (com.rey.material.widget.Button) findViewById(R.id.btn_panic);

        btn_panic.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent otpIntent = new Intent(MainActivity.this, AnxietyActivity.class);
                startActivity(otpIntent);
            }});

        worryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent otpIntent = new Intent(MainActivity.this, WorryBox.class);
                startActivity(otpIntent);
            }});

        goalsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent otpIntent = new Intent(MainActivity.this, GoalsViewpagerActivity.class);
                startActivity(otpIntent);
            }});

        commButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent otpIntent = new Intent(MainActivity.this, CommunicationActivity.class);
                startActivity(otpIntent);
            }});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == R.id.action_stats) {
            Intent otpIntent = new Intent(MainActivity.this, GraphActivity.class);
            startActivity(otpIntent);
        }

        return super.onOptionsItemSelected(item);
    }


}
