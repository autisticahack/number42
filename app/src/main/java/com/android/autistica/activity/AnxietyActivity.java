package com.android.autistica.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import com.android.autistica.R;

public class AnxietyActivity extends BaseActivity {

    Button button;

    ProgressBar progressBar;
    volatile long counter = 1;
    int incrementStep = 10;


    DecrementProgressBarTask decrementTask;
    IncrementProgressBarTask incrementTask;


    boolean isIncrementing = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anxiety);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        progressBar = (ProgressBar) findViewById(R.id.panicbar);

        addListenerOnButton();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();

        System.out.println(" ***** Calling Stop *****");
    }


    private void addListenerOnButton() {


        button = (Button) findViewById(R.id.panic);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (incrementTask == null || incrementTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
                    incrementTask = new IncrementProgressBarTask();
                    incrementTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }

                if (decrementTask == null || decrementTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
                    decrementTask = new DecrementProgressBarTask();
                    decrementTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

    }

    private class IncrementProgressBarTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            System.out.println("In Background for increment");

            counter = counter + incrementStep;
            if (counter > 300) counter = 300;
            publishProgress();

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress((int) counter);
        }
    }


    private class DecrementProgressBarTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            System.out.println("In Background for Decrement");

            while (counter > 1) {
                counter--;

                System.out.println(" ** Decreasing " + counter);

                try {
                    //DecrementProgressBarTask.this.wait(500);
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();

                }

                publishProgress();
                isIncrementing = false;

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress((int) counter);
        }


    }

}
