package com.android.autistica.rest.service;

import com.android.autistica.model.HttpBin;
import com.android.autistica.rest.util.RestCall;
import retrofit2.http.GET;

public interface TestService {

    @GET("/ip")
    RestCall<HttpBin> getIp();
}
