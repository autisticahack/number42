package com.android.autistica.util.adapters;

import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.android.autistica.R;
import com.android.autistica.activity.GoalsFragment;
import com.android.autistica.app.Application;
import com.viewpagerindicator.IconPagerAdapter;

public class GoalsFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "This", "Is", "A", "Test", };

    private static TypedArray goals_imgs = Application.getApplicationInstance().getResources().obtainTypedArray(R.array.goal_images);
    private static TypedArray goals_msg = Application.getApplicationInstance().getResources().obtainTypedArray(R.array.goal_msg);

    protected static final int[] ICONS = new int[] {
            R.drawable.ic_feeling_happy,

    };

    private int mCount = CONTENT.length;

    public GoalsFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        return GoalsFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return goals_imgs.length();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return GoalsFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
