package com.android.autistica.util.adapters;

import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.android.autistica.R;
import com.android.autistica.activity.TestFragment;
import com.android.autistica.app.Application;
import com.viewpagerindicator.IconPagerAdapter;

public class TestFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "This", "Is", "A", "Test", };

    private static TypedArray edu_imgs = Application.getApplicationInstance().getResources().obtainTypedArray(R.array.edu_images);
    private static TypedArray edu_msg = Application.getApplicationInstance().getResources().obtainTypedArray(R.array.edu_msg);

    protected static final int[] ICONS = new int[] {
            R.drawable.ic_feeling_happy,

    };

    private int mCount = CONTENT.length;

    public TestFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        return TestFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return edu_imgs.length();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TestFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
