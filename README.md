# README #

The following android application was developed over 24 hrs as part of the Deutsche Bank COO Hackathon.

It was developed by a team of 9 people from DBOI - Pune.

The team was formed of:

1. Satyaki Basu (Team Lead)
2. Saumitra Joshi
3. Ankur Dongre
4. Chaitanya Karamkar
5. Jayendra Khatod
6. Bhavin Karani
7. Srilata Mitra
8. Vaishali Giri
9. Amey Shirke

App Video can be found at video/ in repo.